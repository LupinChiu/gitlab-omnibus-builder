# GitLab Builder Images

This project builds and publishes Docker images that are used by
the CI pipeline of [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab) to build official GitLab packages. It
contains Dockerfiles for all the official OSs for which packages are provided,
with a few additional Docker images used by various other housekeeping CI jobs in
`omnibus-gitlab`.

Push mirroring is configured from
[GitLab.com repository](https://gitlab.com/gitlab-org/gitlab-omnibus-builder)
to the [dev.gitlab.org mirror](https://dev.gitlab.org/cookbooks/gitlab-omnibus-builder)

## Cutting new releases

To cut a new release:

```
git tag -a VERSION_NUMBER -m "Reason for update"
git push --follow-tags
```

This will trigger a continuous integration job that creates the images, tags
them with `VERSION_NUMBER`, and makes them available in the container
registry.

## License and Authors

Contents of this project are distributed under the MIT license, see LICENSE.
