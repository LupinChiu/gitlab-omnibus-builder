FROM centos:centos7 as builder

# Install required packages
RUN yum update -y -q
RUN yum groupinstall -y Development Tools
RUN yum install -y epel-release
RUN yum install -y -q \
      autoconf \
      automake \
      autopoint \
      gcc \
      gcc-c++ \
      curl-devel \
      expat-devel \
      gettext-devel \
      openssl-devel \
      perl-devel \
      zlib-devel \
      make \
      libyaml-devel \
      libffi-devel \
      readline-devel \
      zlib-devel \
      gdbm-devel \
      ncurses-devel \
      bzip2 \
      which \
      byacc \
      ccache \
      distcc \
      unzip

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
RUN localedef -c -f UTF-8 -i en_US en_US.UTF-8

ENV CMAKE_VERSION 3.18.1
RUN curl -L https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh -o /tmp/cmake-installer.sh \
    && chmod +x /tmp/cmake-installer.sh \
    && /tmp/cmake-installer.sh --prefix=/usr --exclude-subdir --skip-license

ENV AWSCLI_VERSION 2.1.4
RUN curl -Lo /tmp/awscli.zip "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWSCLI_VERSION}.zip" \
  && unzip -qd /tmp/ /tmp/awscli.zip \
  && /tmp/aws/install \
  && aws --version

ENV GIT_VERSION 2.29.0
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-${GIT_VERSION} \
  && ./configure \
  && make all \
  && make install

ENV GO_VERSION 1.14.7
RUN curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz" \
	| tar -xzC /usr/local \
  && ln -sf /usr/local/go/bin/go /usr/local/go/bin/gofmt /usr/local/go/bin/godoc /usr/local/bin/

ENV RUBY_VERSION 2.7.2
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.7/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-${RUBY_VERSION} \
  && ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi\
  && make \
  && make install

ENV RUBYGEMS_VERSION 3.1.4
RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-document

ENV BUNDLER_VERSION 1.17.3
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-document

ENV LICENSE_FINDER_VERSION 6.5.0
RUN /usr/local/bin/gem install license_finder --version ${LICENSE_FINDER_VERSION} --no-document

ENV NODE_VERSION 12.4.0
RUN curl -fsSL "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz" \
  | tar --strip-components 1 -xzC /usr/local/ \
  && node --version

ENV YARN_VERSION 1.16.0
RUN mkdir /usr/local/yarn \
  && curl -fsSL "https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz" \
	| tar -xzC /usr/local/yarn --strip 1 \
  && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin/ \
  && yarn --version

RUN mkdir -p /opt/gitlab /var/cache/omnibus ~/.ssh

RUN git config --global user.email "packages@gitlab.com"
RUN git config --global user.name "GitLab Inc."

RUN rm -rf /tmp/*

FROM centos:centos7
MAINTAINER GitLab Inc. <support@gitlab.com>
COPY --from=builder / /
